<?php $form = $args; ?>


    <?php $form->render('begin'); ?>
    <?php $form->render('errors'); ?>

<div class="askme-wrapper">
    <?php echo $form['question']->label; ?>
    <p><?php echo $form['question']->control; ?></p>
    
    <?php echo $form['yourname']->label; ?>
    <p><?php echo $form['yourname']->control; ?></p>
    
    <?php echo $form['youremail']->label; ?>
    <p><?php echo $form['youremail']->control; ?></p>
    
    <?php echo $form['submit']->control; ?>
</div>



<?php $form->render('end'); ?>