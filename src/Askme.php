<?php 

namespace Gritg;

use Grit\Plugin;
use Nette\Forms\Form;
use Carbon_Fields\Field;
use Carbon_Fields\Container;

class Askme extends Plugin 
{
    protected $question_max_chars = 480;

    protected $name_max_chars = 80;

    protected $update_url = "https://demo.gresak.net/grit-askme/index.php";

    protected $form = false;
    
    public function init()
    {        
        add_shortcode('askme_form',[$this,'askme_form']);
    }
     
    public function setFilters()
    {
        if( ! is_admin()) {
            add_filter('the_posts',[$this, 'initialize_form'],10,1);
        }

        add_action('carbon_fields_register_fields',[$this, 'register_fields']);
    }
    
    public function askme_form()
    {     
        
        if( $this->form ) {
            return $this->form;
        } else {
            return __("The form will appear here.",'grit-askme');
        } 
         
    }
    
    public function initialize_form($posts)
    {
        foreach($posts as $post)
        {
            if(strpos($post->post_content, '[askme_form')) {

                $form = new Form;
                
                $form->addProtection();
                $form->addTextArea('question', __('Question','gritg-askme'))
                    ->addRule($form::MAX_LENGTH,__('Your question is too long.',"grit-askme"), $this->question_max_chars)
                    ->setRequired(true);
                $form->addText('yourname',__("Your name (optional)", 'grit-askme'))
                    ->addRule($form::MAX_LENGTH,__('Your name is too long for this form. Please shorten it.',"grit-askme"), $this->name_max_chars);
                $form->addEmail('youremail', __('Your email(optional)','grit-askme'));
                $form->addSubmit('submit', __('Ask me','grit-askme'));

                if($form->isSuccess()) {
                    $data = $form->getValues();
                    $post = [
                        'post_title'    => $data->question,
                        'post_content'  => "",
                        'post_status'   => 'draft',
                        'post_type'     => 'askme-question'
                    ];
                    $id = wp_insert_post($post);
                    carbon_set_post_meta($id,'asker',$data->yourname);
                    carbon_set_post_meta($id, 'asker_email',$data->youremail);
                    
                    ob_start();
                    $this->load_template('thankyou',$data);
                    echo '<script>
                    if ( window.history.replaceState ) {
                        window.history.replaceState( null, null, window.location.href );
                    }
                    </script>'."\n";
                    $output = ob_get_contents();
                    ob_end_clean();
                    $this->form = $output;
                } else {

                    ob_start();
                    // $form->render();
                    $this->load_template('form',$form);
                    $output = ob_get_contents();
                    ob_end_clean();
                    $this->form = $output;

                }
                
                add_action('wp_enqueue_scripts', function(){
                    wp_enqueue_style('grit-askme',$this->baseurl . "/style.css");
                });

            }
        }

        return $posts;
    }

    public function register_fields()
    {
        Container::make('post_meta', __('Question meta', 'grit-askme'))
            ->where('post_type', "=", 'askme-question')
            ->set_context('side')
            ->add_fields([
                Field::make('text', 'asker', __('Asked by', 'grit-askme')),
                Field::make('text', 'asker_email', __('Asker email','grit-askme'))
            ]);
    }

}