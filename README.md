# Ask Me 

If you use the git version, you should first run `composer install` on your command line.

If you don't have composer installed, check [Get Composer](https://getcomposer.org/) 

## Overriding templates

In your theme create a folder "grit-askme", where you save your custom temlate with the same name, as in the plugins templates folder (just like in WooCommerce).

## Usage

You use the shorttag \[askme_form\] anywhere on your site.