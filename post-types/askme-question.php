<?php

/**
 * Registers the `askme_question` post type.
 */
function askme_question_init() {
	register_post_type(
		'askme-question',
		[
			'labels'                => [
				'name'                  => __( 'Questions', 'grit-askme' ),
				'singular_name'         => __( 'Question', 'grit-askme' ),
				'all_items'             => __( 'All Questions', 'grit-askme' ),
				'archives'              => __( 'Question Archives', 'grit-askme' ),
				'attributes'            => __( 'Question Attributes', 'grit-askme' ),
				'insert_into_item'      => __( 'Insert into Question', 'grit-askme' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Question', 'grit-askme' ),
				'featured_image'        => _x( 'Featured Image', 'askme-question', 'grit-askme' ),
				'set_featured_image'    => _x( 'Set featured image', 'askme-question', 'grit-askme' ),
				'remove_featured_image' => _x( 'Remove featured image', 'askme-question', 'grit-askme' ),
				'use_featured_image'    => _x( 'Use as featured image', 'askme-question', 'grit-askme' ),
				'filter_items_list'     => __( 'Filter Questions list', 'grit-askme' ),
				'items_list_navigation' => __( 'Questions list navigation', 'grit-askme' ),
				'items_list'            => __( 'Questions list', 'grit-askme' ),
				'new_item'              => __( 'New Question', 'grit-askme' ),
				'add_new'               => __( 'Add New', 'grit-askme' ),
				'add_new_item'          => __( 'Add New Question', 'grit-askme' ),
				'edit_item'             => __( 'Edit Question', 'grit-askme' ),
				'view_item'             => __( 'View Question', 'grit-askme' ),
				'view_items'            => __( 'View Questions', 'grit-askme' ),
				'search_items'          => __( 'Search Questions', 'grit-askme' ),
				'not_found'             => __( 'No Questions found', 'grit-askme' ),
				'not_found_in_trash'    => __( 'No Questions found in trash', 'grit-askme' ),
				'parent_item_colon'     => __( 'Parent Question:', 'grit-askme' ),
				'menu_name'             => __( 'Questions', 'grit-askme' ),
			],
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => [ 'title', 'editor' ],
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-format-status',
			'show_in_rest'          => true,
			'rest_base'             => 'askme-question',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		]
	);

}

add_action( 'init', 'askme_question_init' );

/**
 * Sets the post updated messages for the `askme_question` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `askme_question` post type.
 */
function askme_question_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['askme-question'] = [
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Question updated. <a target="_blank" href="%s">View Question</a>', 'grit-askme' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'grit-askme' ),
		3  => __( 'Custom field deleted.', 'grit-askme' ),
		4  => __( 'Question updated.', 'grit-askme' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Question restored to revision from %s', 'grit-askme' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Question published. <a href="%s">View Question</a>', 'grit-askme' ), esc_url( $permalink ) ),
		7  => __( 'Question saved.', 'grit-askme' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Question submitted. <a target="_blank" href="%s">Preview Question</a>', 'grit-askme' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Question scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Question</a>', 'grit-askme' ), date_i18n( __( 'M j, Y @ G:i', 'grit-askme' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Question draft updated. <a target="_blank" href="%s">Preview Question</a>', 'grit-askme' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	];

	return $messages;
}

add_filter( 'post_updated_messages', 'askme_question_updated_messages' );

/**
 * Sets the bulk post updated messages for the `askme_question` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `askme_question` post type.
 */
function askme_question_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['askme-question'] = [
		/* translators: %s: Number of Questions. */
		'updated'   => _n( '%s Question updated.', '%s Questions updated.', $bulk_counts['updated'], 'grit-askme' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Question not updated, somebody is editing it.', 'grit-askme' ) :
						/* translators: %s: Number of Questions. */
						_n( '%s Question not updated, somebody is editing it.', '%s Questions not updated, somebody is editing them.', $bulk_counts['locked'], 'grit-askme' ),
		/* translators: %s: Number of Questions. */
		'deleted'   => _n( '%s Question permanently deleted.', '%s Questions permanently deleted.', $bulk_counts['deleted'], 'grit-askme' ),
		/* translators: %s: Number of Questions. */
		'trashed'   => _n( '%s Question moved to the Trash.', '%s Questions moved to the Trash.', $bulk_counts['trashed'], 'grit-askme' ),
		/* translators: %s: Number of Questions. */
		'untrashed' => _n( '%s Question restored from the Trash.', '%s Questions restored from the Trash.', $bulk_counts['untrashed'], 'grit-askme' ),
	];

	return $bulk_messages;
}

add_filter( 'bulk_post_updated_messages', 'askme_question_bulk_updated_messages', 10, 2 );
