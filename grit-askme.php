<?php
/**
 * Plugin Name:     Ask me by Grit
 * Plugin URI:      https://gresak.net
 * Description:     Ask me a question in a form, and I will answer you in a post.
 * Author:          Gregor Grešak
 * Author URI:      https://gresak.net
 * Text Domain:     grit-askme
 * Domain Path:     /languages
 * Version:         1.0.4
 *
 * @package         Askme
 */

use Grit\Container;

require_once("vendor/autoload.php");

include_once "post-types/askme-question.php";

register_activation_hook(__FILE__, function(){
    flush_rewrite_rules();
});

$container = Grit\Container::getInstance();

$container['gritg.askme'] = new Gritg\Askme(__FILE__);